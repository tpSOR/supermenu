Script bash supermenu

Considerar:

1) Dar permisos de lectura a otros y grupos

$chmod 744 supermenu.sh

2)Como hacer la aplicion de escritorio?

-Abrir terminal (en cualquier parte)
-$sudo gedit /usr/share/applications/supermenu.desktop

-Dentro del gedit, escribir exactamente lo siguiente:

[Desktop entry]
Name = supermenu
Comment = supermenu en bash
Exec = pathAlBash/supermenu.sh  (ejemplo: /home/martindome/ungs/sistemasOperativosYRedes/supermenu/supermenu.sh)
Icon = pathAlIcono/icono        (ejemplo: /home/martindome/ungs/sistemasOperativosYRedes/supermenu/icono)
Terminal = true
Type = Application



Guardar, cerrar y listo. Ya se puede ejecutar el supermenu desde el escritorio.

3) Como usar git? Esta muy bueno. Hay dos formas de trabajar.

-La primera es hacer un grupo y que todos trabajemos el proyecto al mismo tiempo,
sin tocar la rama master, solo trabajando en nuestras branch y cuando este listo, 
mergeamos a la master. El problema es que si todos trabajamos sobre el mismo
proyecto, todos lo podemos modificar, y es complicado

-La segunda es que yo les pase el path a un proyecto mio, ustedes le hacen un fork
y hacen lo que quieren con eso. Se pueden hacer merge request para que el mio
tenga lo mismo y los demas lo puedan utilizar. Tienen que ver como se hace el
Merge Request desde git lab. Es facil. Entran al proyecto, a la izquiera dice merge
request y eligen la branch del fork y la del proyecto original.

Comando basicos:

Loguearse localmente
$git config --global user.name "Juan Perez"
$git config --global user.email "juanperez@yahoo.com"

Ahora vas a GitLab y creas un proyecto (o capaz ya tenias uno)

Clonar proyecto:
$git clone https:/gitlab.com/juanperez/miSupermenu.git
$cd miSupermenu
$git pull origin master  //si el proyecto tenia algo, te lo baja


Hacer de una carpeta un repositorio que accede a uno remoto:
$cd carpetaSupermenu
$git init
$git remote add origin https://gitlab.com/juanperez/miSupermenu.git

Agregar cosas al repositorio local:
$git add archivo   // $git add archivos // $git add *

Prepararlas para subirlas al repositorio remoto:
$git commit -m "Que estoy añadiendo"

Subir commit al repositorio remoto:
$git add origin master   // $git add origin nombreBranch

Verificar si hay cosas para subir:
$git status

Descargar los cambio que se realizaron
$git pull origin nombreBranch  (se recomienda hacer siempre que iniciemos)



Crear una Branch:
$git checkout -b nombreBranch

Meter una brach creado al repositorio remoto:
$git push origin nombreBranch

Moverse de Branch:
$git checkout nombreBranch // $git checkout master

Borrar una brach:
$git checkout -d nombreBranch

Merge master con branch  (master <-- nombreBranch)
$git checkout master
$git merge nombreBranch

Merge branches
$git checkout mergeRecibe
$git checkout mergeTiene

Ver las branches del proyecto
$git branch


:::::Ejemplo completo::::

//Hay un proyecto en gitLab que cree que se llama supermenu
//Tiene un Readme.txt, nada mas

//En mi terminal, en el directorio que me plazaca

$git init                                                           //incio repositorio local
$git add remote origin https://gitlab.com/juanperez/supermenu.git   //linkeo a repositorio remoto
$git pull origin master                                             //me bajo los archivos que tiene master   

//trabajo un poco en mi proyecto. C, python, bash, Threads, lo que sea...
$touch supermenu.sh
$nano supermenu.sh
//Termine la base del supermenu. Quiero subirla a master

$git add supermenu.sh                                               //meto en el repositorio local
$git commit -m "Añado la base del supermenu. Falta completar las funciones" //creo el commit a subir al remoto

$git push origin master

//Termine por hoy. Me fui a dormir//
...
//Vuelvo al otro dia
//Tuve una idea, pero no quiero arruinar master por si falla
//Entonces creo una branch
//Si te da miedo borrar todo, anda a otra carpera, baja de nuevo el proyecto (o clonalo)
//y ahi creas la branch. Master esta en un directorio y branch en otro.

$git checkout -b branchFunciones      //crea una nueva branch. Todavia no esta en el remoto
$git push origin branchFunciones      //ya esta en el proyecto remoto

//TRABAJO EN SUPERMENU//
$nano supermenu.sh
//TERMINE

//Me gusto como quedo y funciona.
//Hago merge

$git checkout master
$git merge branchFunciones

//Ahora se mezclaron.



:::Termino el ejemplo:::

No hay mucho mas que eso. Si le hacen un fork al proyecto, estos son los pasos en su terminal,
una vez que ya hicieron el fork (un fork es una copia del un proyecto. te crea un proyecto para
vos, pero la diferencia es que podes hacer un Merge-Request con el dueño original del proyecto, 
y el le mete los cambios a una branch del proyecto original de una de tu fork. El merge request
se hace desde GitLab).

$git init 
$git remote add origin https://gitlab.com/miUserDeGitlab/proyectoForkeado.git
$git pull origin master // $git pull origin branch

...
$git add *
$git commit -m "Hice un cambio"
$git push origin master

//Ahora le pido a juan perez que acepte el merge request. Se hace desde GitLab 




