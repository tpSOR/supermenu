/*para compilar y ejecutar: gcc -o ejecutable prueba.c -pthread*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/*para crear hijos*/
#include <wait.h>
#include <sys/types.h>

#include <semaphore.h>  //para usar semaforos
#include <pthread.h>   	//para usar threads y mutex

sem_t semaforo_A;
sem_t semaforo_B;
sem_t semaforo_C;
sem_t semaforo_D;
sem_t semaforo_E;
sem_t semaforo_F;

pthread_mutex_t mutex;

void* imp_oleOleOle(void* parametro){

	sem_wait(&semaforo_A);
        sem_wait(&semaforo_B);
	pthread_mutex_lock(&mutex);;
	/*Zona exclusiva*/
	printf("Ole Ole Ole\n");
	/*Fin zona exclusiva*/
	pthread_mutex_unlock(&mutex);
	sem_post(&semaforo_C);
	pthread_exit(NULL);
}

void* imp_oleOleOla(void* parametro){

        sem_wait(&semaforo_C);
        pthread_mutex_lock(&mutex);
        /*Zona exclusiva*/
        printf("Ole Ole Olá\n");
        /*Fin zona exclusiva*/
        pthread_mutex_unlock(&mutex);
	sem_post(&semaforo_A);
        sem_post(&semaforo_B);
        sem_post(&semaforo_C);
        sem_post(&semaforo_D);
       
        pthread_exit(NULL);
}

void* imp_cadaDiaTeQuieroMas(void* parametro){

        sem_wait(&semaforo_D);
        sem_wait(&semaforo_C);
        pthread_mutex_lock(&mutex);
        /*Zona exclusiva*/
        printf("cada dia te quiero más\n");
        /*Fin zona exclusiva*/
        pthread_mutex_unlock(&mutex);
        sem_post(&semaforo_E);
        pthread_exit(NULL);
}
void* imp_oohArgentina(void* parametro){

        sem_wait(&semaforo_E);
        pthread_mutex_lock(&mutex);
        /*Zona exclusiva*/
        printf("ooh Argentina\n");
        /*Fin zona exclusiva*/
        pthread_mutex_unlock(&mutex);
        sem_post(&semaforo_F);
        pthread_exit(NULL);
}
void* imp_esUnSentimiento(void* parametro){

        sem_wait(&semaforo_F);
        pthread_mutex_lock(&mutex);
        /*Zona exclusiva*/
        printf("es un sentimiento, no puedo parar...\n");
        /*Fin zona exclusiva*/
        pthread_mutex_unlock(&mutex);
        sem_post(&semaforo_A);
        sem_post(&semaforo_B);
        pthread_exit(NULL);
}
int main(){
       

	int rc, value;

	pthread_mutex_init(&mutex,NULL);
	sem_init(&semaforo_A, 0, 1);
	sem_init(&semaforo_B, 0, 1);
	sem_init(&semaforo_C, 0, 0);
        sem_init(&semaforo_D, 0, 0);
	sem_init(&semaforo_E, 0, 0);
	sem_init(&semaforo_F, 0, 0);
	
        pthread_t oleOleOle;
	pthread_t oleOleOla;
	pthread_t cadaDiaTeQuieroMas;
        pthread_t oohArgentina;
        pthread_t esUnSentimiento;

	


	rc = pthread_create(&oleOleOla, NULL, imp_oleOleOla, NULL);
	rc = pthread_create(&cadaDiaTeQuieroMas, NULL, imp_cadaDiaTeQuieroMas, NULL);
	rc = pthread_create(&oohArgentina, NULL, imp_oohArgentina, NULL);
	rc = pthread_create(&esUnSentimiento, NULL, imp_esUnSentimiento, NULL);
	rc = pthread_create(&oleOleOle, NULL, imp_oleOleOle, NULL);


	//rc = pthread_join(ho, NULL);
	//rc = pthread_join(lan, NULL);
	//rc = pthread_join(da, NULL);


	pthread_mutex_destroy(&mutex);
	sem_destroy(&semaforo_A);
	sem_destroy(&semaforo_B);
	sem_destroy(&semaforo_C);
        sem_destroy(&semaforo_D);
	sem_destroy(&semaforo_E);
	sem_destroy(&semaforo_F);

	pthread_exit(NULL);

	return 0;
}










